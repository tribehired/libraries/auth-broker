<?php

use Illuminate\Support\Facades\Route;
use Tribehired\AuthBroker\Controllers\LoginController;

Route::middleware('web')->group(function () {

    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

    Route::middleware('guest:sso')->group(function () {
        Route::get('/login', function () {
            $ssoUrl = config('tribehired-broker.SSO_SERVER');
            $homeRoute = route('home');

            return redirect("{$ssoUrl}/login/tribehired?homeUrl={$homeRoute}");
        })->name('login');

        Route::get('/register/talent', function () {
            $ssoUrl = config('tribehired-broker.SSO_SERVER');
            $homeRoute = route('home');

            $query = array_merge(
                request()->query(),
                [
                    'homeUrl' => $homeRoute
                ]
            );

            $url = "{$ssoUrl}/register/tribehired/talent";

            return redirect($url . '?' . http_build_query($query));
        })->name('talent-register');

        Route::get('/register/employer', function () {
            $ssoUrl = config('tribehired-broker.SSO_SERVER');
            $homeRoute = route('home');

            $query = array_merge(
                request()->query(),
                [
                    'homeUrl' => $homeRoute
                ]
            );

            $url = "{$ssoUrl}/register/tribehired/employer";

            return redirect($url . '?' . http_build_query($query));
        })->name('employer-register');
    });
});
