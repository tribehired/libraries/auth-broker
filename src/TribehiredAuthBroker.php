<?php

namespace Tribehired\AuthBroker;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Tribehired\AuthBroker\Helpers\UrlHelper;

class TribehiredAuthBroker
{
    /**
     * @var Client $client
     */
    protected $client;

    /**
     * @var String $token
     */
    protected $token;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function setToken(String $token): void
    {
        $this->token = $token;
    }

    public function attach()
    {
        if (!$this->token) {
            throw new \Exception("Token does not exist");
        }

        try {
            $res = $this->client->post(UrlHelper::getBaseUrl() . "/api/sso/attach", [
                RequestOptions::JSON => [
                    "token" => $this->token,
                ]
            ]);

            return $res->getBody()->getContents();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateEmail($oldEmail, $newEmail)
    {
        $this->client->post(UrlHelper::getBaseUrl() . "/api/change-email", [
            RequestOptions::JSON => [
                'oldEmail' => $oldEmail,
                'newEmail' => $newEmail,
            ]
        ]);
    }

    public function getToken()
    {
        return $this->token;
    }

    public function checkUserExists($encryptedEmail)
    {
        try {
            $res = $this->client->post(UrlHelper::getBaseUrl() . "/api/sso/check-user", [
                RequestOptions::JSON => [
                    'encrypted-data' => $encryptedEmail
                ]
            ]);

            if ($res->getStatusCode() !== 200) {
                throw new \Exception('Request Failed');
            }

            return $res->getBody()->getContents();
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function redirectLogin(string $returnUrl)
    {
        $uri = new Uri(config('tribehired-broker.SSO_SERVER'));
        $uri = $uri->withPath("/sso/login");
        $uri = $uri->withQueryValue($uri, "returnUrl", $returnUrl);

        redirect()->away($uri)->send();
    }
}
