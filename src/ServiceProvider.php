<?php

namespace Tribehired\AuthBroker;

use Illuminate\Support\Facades\Route;
use Tribehired\AuthBroker\TribehiredEncryption;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tribehiredencryption', function ($app) {
            return new TribehiredEncryption(config('tribehired-broker.OPEN_SSL_PASSWORD'), config('tribehired-broker.OPEN_SSL_VECTOR'));
        });

        $this->mergeConfigFrom(
            __DIR__ . '/config/tribehired-broker.php',
            'tribehired-broker.php'
        );
    }

    public function provides()
    {
        return ['tribehiredencryption'];
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/tribehired-broker.php' => config_path('tribehired-broker.php'),
        ]);

        $this->registerRoutes();
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/routes.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => config('tribehired-broker.ROUTE_PREFIX'),
        ];
    }
}
