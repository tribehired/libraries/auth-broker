<?php

return [
    'OPEN_SSL_PASSWORD' => env('OPEN_SSL_PASSWORD', false),
    'OPEN_SSL_VECTOR' => env('OPEN_SSL_VECTOR', false),
    'SSO_SERVER' => env("SSO_SERVER", "https://auth.tribehired.com"),
    'DOCKER_SSO_NAME' => env("DOCKER_SSO_NAME"),
    'ROUTE_PREFIX' => 'auth-broker',
    'ROUTE_MIDDLEWARE' => ['web', 'guest:sso']
];
