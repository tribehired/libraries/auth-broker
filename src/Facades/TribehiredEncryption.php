<?php

namespace Tribehired\AuthBroker\Facades;

class TribehiredEncryption extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tribehiredencryption';
    }
}
