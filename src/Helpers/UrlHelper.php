<?php

namespace Tribehired\AuthBroker\Helpers;

class UrlHelper
{
    public static function getBaseUrl()
    {
        if (!empty(config('tribehired-broker.DOCKER_SSO_NAME'))) {
            return config('tribehired-broker.DOCKER_SSO_NAME');
        }

        return config('tribehired-broker.SSO_SERVER');
    }
}
