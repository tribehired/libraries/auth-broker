<?php

namespace Tribehired\AuthBroker;

class TribehiredEncryption
{
    private $openSslPassword;
    private $openSslVector;

    public function __construct($openSslPassword, $openSslVector)
    {
        $this->openSslPassword = $openSslPassword;
        $this->openSslVector = $openSslVector;
    }

    private function paramBuilder()
    {
        if (empty($this->openSslPassword) || empty($this->openSslVector)) {
            throw new \Exception('Unable to encrypt due to missing variables');
        }

        return [
            $this->openSslPassword,
            $this->openSslVector,
            "aes-128-cbc"
        ];
    }

    public function encryptData($data)
    {
        [$pass, $iv, $method] = $this->paramBuilder();

        $string = json_encode($data);

        $encrypted = openssl_encrypt($string, $method, $pass, false, $iv);

        return rawurlencode(base64_encode($encrypted));
    }

    public function decryptData($encryptedData)
    {
        [$pass, $iv, $method] = $this->paramBuilder();

        $string = base64_decode(rawurldecode($encryptedData));

        $decryptedData = openssl_decrypt($string, $method, $pass, false, $iv);

        return json_decode($decryptedData);
    }
}
