<?php

namespace Tribehired\AuthBroker\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Tribehired\AuthBroker\Helpers\UrlHelper;

class LoginController extends Controller
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function logout(Request $request)
    {
        try {
            $res = $this->client->post(UrlHelper::getBaseUrl() . "/api/logout");

            if ($res->getStatusCode() == 200) {
                $this->guard()->logout();
                Cookie::queue(Cookie::forget('user'));

                return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect('/');
            } else {
                throw new \Exception('Failed to logout. Request unsuccessful');
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
